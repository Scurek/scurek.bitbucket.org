
var baseUrl = 'https://rest.ehrscape.com/rest/v1';
var queryUrl = baseUrl + '/query';

var username = "ois.seminar";
var password = "ois4fri";
//var authorization = "Basic " + btoa(username + ":" + password)
//var teze;
var trenutniID = "";
var test1ID = "9c437440-31af-4f04-9b7a-8c3332717d9e";
var test2ID = "c7ece5e3-ad63-4d0b-9d28-d9f31e3b797a";
var test3ID = "98cfcd90-6c1a-40b8-8e50-ef642b8a1209";
//var test4ID = "54a6dd28-e357-4054-b363-c272b8b59449";
//var test5ID = "5cdebdfa-0cd7-4d85-a1c1-dad97cee3da3";
var zadnjiBMI = 0;
//var Graf = null;
//var GrafBMI = null;
var myChart = null;
var myChart2 = null;
/**
 * Prijava v sistem z privzetim uporabnikom za predmet OIS in pridobitev
 * enolične ID številke za dostop do funkcionalnosti
 * @return enolični identifikator seje za dostop do funkcionalnosti
 */
function getSessionId() {
    var response = $.ajax({
        type: "POST",
        url: baseUrl + "/session?username=" + encodeURIComponent(username) +
                "&password=" + encodeURIComponent(password),
        async: false
    });
    return response.responseJSON.sessionId;
}


/**
 * Generator podatkov za novega pacienta, ki bo uporabljal aplikacijo. Pri
 * generiranju podatkov je potrebno najprej kreirati novega pacienta z
 * določenimi osebnimi podatki (ime, priimek in datum rojstva) ter za njega
 * shraniti nekaj podatkov o vitalnih znakih.
 * @param stPacienta zaporedna številka pacienta (1, 2 ali 3)
 * @return ehrId generiranega pacienta
 */
 

 
function generirajPodatke(stPacienta) {
	//console.log ("lala");
	sessionId = getSessionId();
	//var ehrId = "";
	$.ajaxSetup({
		headers: {"Ehr-Session": sessionId}
	});
	$.ajax({
	    url: baseUrl + "/ehr",
	    type: 'POST',
	    success: function (data) {
	        var ehrId = data.ehrId;
	        //$("#header").html("EHR: " + ehrId);
			//$("#Generiranec1").innerHTML = ehrId;
	        // build party data
	        if (stPacienta == 1) {
		        var partyData = {
		            firstNames: "Janko",
		            lastNames: "Franko",
		            gender: "MALE",
		            dateOfBirth: "2002-07-18",
		            partyAdditionalInfo: [
		                {
		                    key: "ehrId",
		                    value: ehrId
		                }
		            ]
		        };
		        var podatki = [
		        	{
					    "ctx/language": "en",
					    "ctx/territory": "SI",
					    "ctx/time": "1990-11-21T11:40:00.000+01:00",
					    "vital_signs/height_length/any_event/body_height_length": "150",
					    "vital_signs/body_weight/any_event/body_weight": "60"
		        	},
					{
					    "ctx/language": "en",
					    "ctx/territory": "SI",
					    "ctx/time": "1991-11-21T11:40Z",
					    "vital_signs/height_length/any_event/body_height_length": "151",
					    "vital_signs/body_weight/any_event/body_weight": "61"
					},
					{
					    "ctx/language": "en",
					    "ctx/territory": "SI",
					    "ctx/time": "1992-11-21T11:40Z",
					    "vital_signs/height_length/any_event/body_height_length": "152",
					    "vital_signs/body_weight/any_event/body_weight": "62"
					},
					{
					    "ctx/language": "en",
					    "ctx/territory": "SI",
					    "ctx/time": "1993-11-21T11:40Z",
					    "vital_signs/height_length/any_event/body_height_length": "152",
					    "vital_signs/body_weight/any_event/body_weight": "60"
					},
					{
					    "ctx/language": "en",
					    "ctx/territory": "SI",
					    "ctx/time": "1994-11-21T11:40Z",
					    "vital_signs/height_length/any_event/body_height_length": "153",
					    "vital_signs/body_weight/any_event/body_weight": "40"
					}
					
	        	];
	        }
	        else if (stPacienta == 2) {
		        var partyData = {
		            firstNames: "Francka",
		            lastNames: "Zavaljena",
		            gender: "FEMALE",
		            dateOfBirth: "1970-07-18",
		            partyAdditionalInfo: [
		                {
		                    key: "ehrId",
		                    value: ehrId
		                }
		            ]
		        };
		        var podatki = [
		        	{
					    "ctx/language": "en",
					    "ctx/territory": "SI",
					    "ctx/time": "1980-11-21T11:40Z",
					    "vital_signs/height_length/any_event/body_height_length": "165",
					    "vital_signs/body_weight/any_event/body_weight": "80",
		        	},
					{
					    "ctx/language": "en",
					    "ctx/territory": "SI",
					    "ctx/time": "1990-11-21T11:40Z",
					    "vital_signs/height_length/any_event/body_height_length": "166",
					    "vital_signs/body_weight/any_event/body_weight": "90",
					},
					{
					    "ctx/language": "en",
					    "ctx/territory": "SI",
					    "ctx/time": "2000-11-21T11:40Z",
					    "vital_signs/height_length/any_event/body_height_length": "167",
					    "vital_signs/body_weight/any_event/body_weight": "100",
					},
					{
					    "ctx/language": "en",
					    "ctx/territory": "SI",
					    "ctx/time": "2010-11-21T11:40Z",
					    "vital_signs/height_length/any_event/body_height_length": "167",
					    "vital_signs/body_weight/any_event/body_weight": "110",
					},
					{
					    "ctx/language": "en",
					    "ctx/territory": "SI",
					    "ctx/time": "2018-01-01T11:40Z",
					    "vital_signs/height_length/any_event/body_height_length": "168",
					    "vital_signs/body_weight/any_event/body_weight": "120",
					}
	        	];
	        }
	        else if (stPacienta == 3) {
		        var partyData = {
		            firstNames: "Gandalf",
		            lastNames: "the Gray",
		            gender: "MALE",
		            dateOfBirth: "1000-01-01",
		            partyAdditionalInfo: [
		                {
		                    key: "ehrId",
		                    value: ehrId
		                }
		            ]
		        };
		        var podatki = [
		        	{
					    "ctx/language": "en",
					    "ctx/territory": "SI",
					    "ctx/time": "1980-11-21T11:40Z",
					    "vital_signs/height_length/any_event/body_height_length": "180.34",
					    "vital_signs/body_weight/any_event/body_weight": "69"
		        	},
					{
					    "ctx/language": "en",
					    "ctx/territory": "SI",
					    "ctx/time": "1990-11-21T11:40Z",
					    "vital_signs/height_length/any_event/body_height_length": "180.34",
					    "vital_signs/body_weight/any_event/body_weight": "69"
					},
					{
					    "ctx/language": "en",
					    "ctx/territory": "SI",
					    "ctx/time": "2000-11-21T11:40Z",
					    "vital_signs/height_length/any_event/body_height_length": "180.34",
					    "vital_signs/body_weight/any_event/body_weight": "69"
					},
					{
					    "ctx/language": "en",
					    "ctx/territory": "SI",
					    "ctx/time": "2010-11-21T11:40Z",
					    "vital_signs/height_length/any_event/body_height_length": "180.34",
					    "vital_signs/body_weight/any_event/body_weight": "69"
					},
					{
					    "ctx/language": "en",
					    "ctx/territory": "SI",
					    "ctx/time": "2018-01-01T11:40Z",
					    "vital_signs/height_length/any_event/body_height_length": "180.34",
					    "vital_signs/body_weight/any_event/body_weight": "69"
					}
	        	];
	        }
	        if (podatki){
		        var parametriZahteve = {
				    ehrId: ehrId,
				    templateId: 'Vital Signs',
				    format: 'FLAT',
				    //committer: merilec
				};
				for (var i = 0; i<podatki.length; i++){
			        $.ajax({
					    url: baseUrl + "/composition?" + $.param(parametriZahteve),
					    type: 'POST',
					    contentType: 'application/json',
					    data: JSON.stringify(podatki[i]),
					 
					    success: function (res) {
					    },
					    error: function(err) {
					    	$("#noviGeneriraneci").html(
				            "<span class='obvestilo label label-danger fade-in'>Napaka '" +
				            JSON.parse(err.responseText).userMessage + "'!");
					    }
					});
	    		}
	        }
	        //console.log(partyData);
	        $.ajax({
	            url: baseUrl + "/demographics/party",
	            type: 'POST',
	            contentType: 'application/json',
	            data: JSON.stringify(partyData),
	            success: function (party) {
                	if (stPacienta == 1){
                		test1ID = ehrId;
                		$("#Generiranec1").html("Janko Franko: " + "<i>"+ehrId+"</i>");
                		if ($("#noviGeneriraneci").val() == "")
                			$("#noviGeneriraneci").html("<span class='obvestilo label label-info fade-in'>Generirani EhrId-ji, prav tako posodobljeni na padajočem meniju:</span>");
                		$("#novGeneriranec1").html("<span class='obvestilo label label-warning fade-in'>Janko Franko: " + ehrId +"</span>");
                	}
                	else if (stPacienta == 2){
                		test2ID = ehrId;
                		$("#Generiranec2").html("Francka Zavaljena: " + "<i>"+ehrId+"</i>");
                		if ($("#noviGeneriraneci").val() == "")
                			$("#noviGeneriraneci").html("<span class='obvestilo label label-info fade-in'>Generirani EhrId-ji, prav tako posodobljeni na padajočem meniju:</span>");
                		$("#novGeneriranec2").html("<span class='obvestilo label label-warning fade-in'>Francka Zavaljena: " + ehrId +"</span>");
                	}
                	else if (stPacienta == 3){
                		test3ID = ehrId;
                		$("#Generiranec3").html("Gandalf the Gray: " + "<i>"+ehrId+"</i>");
                		if ($("#noviGeneriraneci").val() == "")
                			$("#noviGeneriraneci").html("<span class='obvestilo label label-info fade-in'>Generirani EhrId-ji, prav tako posodobljeni na padajočem meniju:</span>");
                		$("#novGeneriranec3").html("<span class='obvestilo label label-warning fade-in'>Gandalf the Gray: " + ehrId +"</span>");
                	}
					return ehrId;
	            },
	            error: function(err) {
		            return "Napaka";
		        }
	        });
	        
	    }
	});
  // TODO: Potrebno implementirati
}

/*function GumbGenerirajPodatke() {
	var ehr1 = generirajPodatke(1);
	console.log("Janko Franko: " + ehr1);
	//$("#Generiranec1").html("Janko Franko: " + ehr1);
	//$("#Generiranec2").html("Francelj Zavaljeni: " + generirajPodatke(2));
	//$("#Generiranec3").html("Gandalf the Gray: " + generirajPodatke(3));
}*/

// TODO: Tukaj implementirate funkcionalnost, ki jo podpira vaša aplikacija
// Nastavi globalen EHRID
function nastaviEHRBolnika(tip) {
	var ehrId;
	if (tip == 0)
		ehrId = $("#nastaviEHRid").val();
	else if (tip == 1){
		ehrId = test1ID;
	//	console.log($("#Generiranec1").val());
	}
	else if (tip == 2)
		ehrId = test2ID;
	else if (tip == 3)
		ehrId = test3ID;
	/*else if (tip == 4)
		ehrId = test4ID;
	else if (tip == 5)
		ehrId = test5ID;*/
	else if (trenutniID == "")
		return;
	else if (tip == 6)
		ehrId = trenutniID;
	
	//narisiOsnovniGUI(ehrId);
	sessionId = getSessionId();
	$.ajax({
	    url: baseUrl + "/demographics/ehr/" + ehrId + "/party",
	    type: 'GET',
	    headers: {
	        "Ehr-Session": sessionId
	    },
	    success: function (data) {
	        var party = data.party;
	
	        // Name
	        $("#patient-name").html("<span style='color:blue;' ><h3>"+party.firstNames + ' ' + party.lastNames+"</h3></span>");
	
	        // Complete age
	        //var age = getAge(formatDateUS(party.dateOfBirth));
	       // $(".patient-age").html(age);
	
	        // Date of birth
	        $("#pacientovID").html("<i>EhrID:</i> "+ ehrId);
	        if (party.dateOfBirth){
		        var date = new Date(party.dateOfBirth);
		        var datum =  date.toLocaleDateString();
		        $("#patient-dob").html("<i>Datum Rojsva:</i> "+datum);
	        }
	        else {
	        	 $("#patient-dob").html("<i>Datum Rojsva:</i> Neznan");
	        }
	        // Gender
	        if (party.gender){
		        var gender = party.gender;
		        if (gender == "MALE")
		        	gender = "moški";
		        if (gender == "FEMALE")
		        	gender = "ženski";
		        if (gender == "UNKNOWN")
		        	gender = "neznano";
		        if (gender == "OTHER")
		        	gender = "drugo";
		        $("#patient-gender").html("<i>Spol:</i> "+gender);
	        }
	        else {
	        	 $("#patient-gender").html("<i>Spol:</i> Neznan");
	        }
	        trenutniID = ehrId;
			$("#mestoZaGumb").html("<button type='button' class='btn btn-success btn-sm' onclick='prehrambek()'>Išči</button>");
			izpisiTeze(ehrId);
	    },
	    error: function(err) {
		    $("#preberiSporocilo").html("<span class='obvestilo label label-warning " +
    			"fade-in'>Neznan EhrId");
		}
	});
	
}
/**
 * Kreiraj nov EHR zapis za pacienta in dodaj osnovne demografske podatke.
 * V primeru uspešne akcije izpiši sporočilo s pridobljenim EHR ID, sicer
 * izpiši napako.
 */
function narisiOsnovniGUI(ehrId) {
	
}

/*var podatkiZaGraf = [
  {
    "unit": "kg",
    "weight": 0,
    "time": "6000-00-00T00:00:00.000+00:00"
  },
  {
    "unit": "kg",
    "weight": 0,
    "time": "5000-00-00T00:00:00.000+00:00"
  },
  {
    "unit": "kg",
    "weight": 0,
    "time": "4000-00-00T00:00:00.000+00:00"
  },
  {
    "unit": "kg",
    "weight": 0,
    "time": "3000-00-00T00:00:00.000+00:00"
  },
  {
    "unit": "kg",
    "weight": 0,
    "time": "2000-00-00T00:00:00.000+00:00"
  },
  {
    "unit": "kg",
    "weight": 0,
    "time": "1000-00-00T00:00:00.000+00:00"
  }
];*/
/*
var podatkiZaGrafBMI = [
  {
    "unit": "kg/m^2",
    "BMI": 0,
    "BMI_MAKS": 20,
    "time": "6000-00-00T00:00:00.000+00:00"
  },
  {
    "unit": "kg/m^2",
    "BMI": 0,
    "BMI_MAKS": 20,
    "time": "5000-00-00T00:00:00.000+00:00"
  },
  {
    "unit": "kg/m^2",
    "BMI": 0,
    "BMI_MAKS": 20,
    "time": "4000-00-00T00:00:00.000+00:00"
  },
  {
    "unit": "kg/m^2",
    "BMI": 0,
    "BMI_MAKS": 20,
    "time": "3000-00-00T00:00:00.000+00:00"
  },
  {
    "unit": "kg/m^2",
    "BMI": 0,
    "BMI_MAKS": 20,
    "time": "2000-00-00T00:00:00.000+00:00"
  },
  {
    "unit": "kg/m^2",
    "BMI": 0,
    "BMI_MAKS": 20,
    "time": "1000-00-00T00:00:00.000+00:00"
  }
];*/

/*var podatkiZaGrafBMI = [
  {
    "x": "2000-01-01T01:01:01.000+00:00",
    "y": 0
  },
  {
    "x": "2001-01-01T01:01:01.000+00:00",
    "y": 0,
    
  },
  {
    "x": "2002-01-01T01:01:01.000+00:00",
    "y": 0,
  }
];*/
var podatkiZaGrafBMI = [];
var podatkiZaGrafTeza = [];
var podatkiZaGrafVisina = [];
function izpisiTeze(ehrId) {
	sessionId = getSessionId();
	$.ajax({
		url: baseUrl + "/demographics/ehr/" + ehrId + "/party",
    	type: 'GET',
    	headers: {"Ehr-Session": sessionId},
    	success: function (data) {
			
			var party = data.party;
			$("#rezultatMeritveVitalnihZnakov").html("<br/><span>Pridobivanje " +
			     "podatkov  <b>teže</b> bolnika <b>'" + party.firstNames +
			     " " + party.lastNames + "'</b>.</span><br/><br/>");
			var tabVisin;
			$.ajax({
				url: baseUrl + "/view/" + ehrId + "/" + "height",
			    type: 'GET',
			    headers: {"Ehr-Session": sessionId},
				success: function (res) {
					tabVisin = res;
					$.ajax({
					    url: baseUrl + "/view/" + ehrId + "/" + "weight",
					    type: 'GET',
					    headers: {"Ehr-Session": sessionId},
					    success: function (res) {
					    	
					    	if (res.length > 0) {
					    		
						    	var results = "<table class='table table-striped " +
		            							"table-hovedar'><tr><th>Datum in ura</th>" +
		            							"<th class='text-right'>Telesna višina</th><th class='text-right'>Telesna teža</th><th class='text-right'>BMI</th></tr>";
						        var teza = res[0].weight;
								var visina = tabVisin[0].height;
								var novGrafBMI = [];
								var novGrafTeze = [];
								var novGrafVisine = [];
								var imaTezo = false;
								zadnjiBMI = ((teza*10000)/(visina*visina)).toFixed(2);
						        for (var i = 0; i<res.length && i<tabVisin.length; i++) {
						        	var j = 0;
						        	//console.log (res[i].weight+" "+tabVisin[i].height);
									
									
									imaTezo = true;
									//if (tabVisin[i]){
										visina = tabVisin[i].height;
										
										teza = res[i].weight;
										var BMI = ((teza*10000)/(visina*visina)).toFixed(2);
										novGrafVisine.push(
											{
											    "x": res[i].time,
											    "y": visina
										
											}
										);
										novGrafTeze.push(
											{
											    "x": res[i].time,
											    "y": teza
										
											}
										);
										novGrafBMI.push(
											{
											    "x": res[i].time,
											    "y": BMI
										
											}
										);
							        	var datum = new Date(res[i].time);
							            results += "<tr><td>" + datum.toLocaleDateString() + "</td><td class='text-right'>" + tabVisin[i].height + " " + tabVisin[i].unit + "</td><td class='text-right'>" + res[i].weight + " " + res[i].unit + "</td><td class='text-right'>" + BMI + " kg/m2</td></tr>";
									//}
									//else
									//	break;
						        }
						        res.forEach(function (el, i, arr) {
						            var date = new Date(el.time);
						            el.date = date.getTime();
						        });
						        results += "</table>";
						        $("#rezultatMeritveVitalnihZnakov").html("");
						        $("#preberiMeritveVitalnihZnakovSporocilo").html("");
						        $("#rezultatMeritveVitalnihZnakov").append(results);
					    	} else {
					    		$("#rezultatMeritveVitalnihZnakov").html("");
					    		$("#preberiMeritveVitalnihZnakovSporocilo").html(
		            "<span class='obvestilo label label-warning fade-in'>" +
		            "Ni podatkov!</span>");
					    	}
					    	
					    	if (!imaTezo){
					    		//Graf.setData(podatkiZaGraf);
					    		//GrafBMI.setData(podatkiZaGrafBMI);
					    		$(".narisiGrafTezeObvestilo").html("<span class='obvestilo label label-warning fade-in'>Ni podatkov!</span>");
					    	}
					    	else {
					    		//Graf.setData(res);
					    		//GrafBMI.setData(novGrafBMI);
					    		//podatkiZaGrafBMI = novGrafBMI;
					    		myChart.data.datasets[0].data = novGrafBMI;
					    		//console.log(podatkiZaGrafBMI);
					    		myChart.update();
					    		myChart2.data.datasets[0].data = novGrafTeze;
					    		myChart2.data.datasets[1].data = novGrafVisine;
					    		myChart2.update();
					    		$(".narisiGrafTezeObvestilo").html("");
					    	}
							if (teza != 0 && visina != 0){
								$("#BMI").html("BMI:" +zadnjiBMI);
								if (zadnjiBMI > 30)
									$("#klasifikacija").html("Debelost");
								else if (zadnjiBMI > 25)
									$("#klasifikacija").html("Povišana telesna teža");
								else if (zadnjiBMI > 18.50)
									$("#klasifikacija").html("Normalna telesna teža");
								else
									$("#klasifikacija").html("Podhranjenost");
							}
							else {
								$("#BMI").html("Ni dovolj podatkov za izračun BMI");
							}
					    	
					    },
					    error: function() {
					    	$("#preberiMeritveVitalnihZnakovSporocilo").html(
		          "<span class='obvestilo label label-danger fade-in'>Napaka '" +
		          JSON.parse(err.responseText).userMessage + "'!");
					    }
					});
				}
			});
			
			
    	},
    	error: function(err) {
    		$("#preberiMeritveVitalnihZnakovSporocilo").html(
        "<span class='obvestilo label label-danger fade-in'>Napaka '" +
        JSON.parse(err.responseText).userMessage + "'!");
    	}
	});
}


function kreirajEHRzaBolnika() {
	sessionId = getSessionId();

	var ime = $("#kreirajIme").val();
	var priimek = $("#kreirajPriimek").val();
	var datumRojstva = $("#kreirajDatumRojstva").val() + "T00:00:00.000Z";

	if (!ime || !priimek || !datumRojstva || ime.trim().length == 0 ||
      priimek.trim().length == 0 || datumRojstva.trim().length == 0) {
		$("#kreirajSporocilo").html("<span class='obvestilo label " +
      "label-warning fade-in'>Prosim vnesite zahtevane podatke!</span>");
	} else {
		$.ajaxSetup({
		    headers: {"Ehr-Session": sessionId}
		});
		$.ajax({
		    url: baseUrl + "/ehr",
		    type: 'POST',
		    success: function (data) {
		        var ehrId = data.ehrId;
		        var partyData = {
		            firstNames: ime,
		            lastNames: priimek,
		            dateOfBirth: datumRojstva,
		            partyAdditionalInfo: [{key: "ehrId", value: ehrId}]
		        };
		        $.ajax({
		            url: baseUrl + "/demographics/party",
		            type: 'POST',
		            contentType: 'application/json',
		            data: JSON.stringify(partyData),
		            success: function (party) {
		                if (party.action == 'CREATE') {
		                    $("#kreirajSporocilo").html("<span class='obvestilo " +
                          "label label-success fade-in'>Uspešno kreiran EHR '" +
                          ehrId + "'.</span>");
		                    $("#preberiEHRid").val(ehrId);
		                }
		            },
		            error: function(err) {
		            	$("#kreirajSporocilo").html("<span class='obvestilo label " +
                    "label-danger fade-in'>Napaka '" +
                    err.responseText + "'!")
		            }
		        });
		    }
		});
	}
}

/**
 * Za podan EHR ID preberi demografske podrobnosti pacienta in izpiši sporočilo
 * s pridobljenimi podatki (ime, priimek in datum rojstva).
 */
 /*
function preberiEHRodBolnika() {
	sessionId = getSessionId();

	var ehrId = $("#preberiEHRid").val();

	if (!ehrId || ehrId.trim().length == 0) {
		$("#preberiSporocilo").html("<span class='obvestilo label label-warning " +
      "fade-in'>Prosim vnesite zahtevan podatek!");
	} else {
		$.ajax({
			url: baseUrl + "/demographics/ehr/" + ehrId + "/party",
			type: 'GET',
			headers: {"Ehr-Session": sessionId},
	    	success: function (data) {
				var party = data.party;
				$("#preberiSporocilo").html("<span class='obvestilo label " +
          "label-success fade-in'>Bolnik '" + party.firstNames + " " +
          party.lastNames + "', ki se je rodil '" + party.dateOfBirth +
          "'.</span>");
			},
			error: function(err) {
				$("#preberiSporocilo").html("<span class='obvestilo label " +
          "label-danger fade-in'>Napaka '" +
          JSON.parse(err.responseText).userMessage + "'!");
			}
		});
	}
}
*/
//var monthNames = ['Jan', 'Feb', 'Mar', 'Apr', 'Maj', 'Jun', 'Jul', 'Aug', 'Sep', 'Okt', 'Nov', 'Dec'];
/**
 * Za dodajanje vitalnih znakov pacienta je pripravljena kompozicija, ki
 * vključuje množico meritev vitalnih znakov (EHR ID, datum in ura,
 * telesna višina, telesna teža, sistolični in diastolični krvni tlak,
 * nasičenost krvi s kisikom in merilec).
 */
function dodajMeritveVitalnihZnakov() {
	sessionId = getSessionId();

	var ehrId = $("#dodajVitalnoEHR").val();
	var datumInUra = $("#dodajVitalnoDatumInUra").val();
	var telesnaVisina = $("#dodajVitalnoTelesnaVisina").val();
	var telesnaTeza = $("#dodajVitalnoTelesnaTeza").val();
	/*var telesnaTemperatura = $("#dodajVitalnoTelesnaTemperatura").val();
	var sistolicniKrvniTlak = $("#dodajVitalnoKrvniTlakSistolicni").val();
	var diastolicniKrvniTlak = $("#dodajVitalnoKrvniTlakDiastolicni").val();
	var nasicenostKrviSKisikom = $("#dodajVitalnoNasicenostKrviSKisikom").val();
	var merilec = $("#dodajVitalnoMerilec").val();*/

	if (!ehrId || ehrId.trim().length == 0) {
		$("#dodajMeritveVitalnihZnakovSporocilo").html("<span class='obvestilo " +
      "label label-warning fade-in'>Prosim vnesite zahtevane podatke!</span>");
	} else {
		$.ajaxSetup({
		    headers: {"Ehr-Session": sessionId}
		});
		var podatki = {
			// Struktura predloge je na voljo na naslednjem spletnem naslovu:
      // https://rest.ehrscape.com/rest/v1/template/Vital%20Signs/example
		    "ctx/language": "en",
		    "ctx/territory": "SI",
		    "ctx/time": datumInUra,
		    "vital_signs/height_length/any_event/body_height_length": telesnaVisina,
		    "vital_signs/body_weight/any_event/body_weight": telesnaTeza
		   	/*"vital_signs/body_temperature/any_event/temperature|magnitude": telesnaTemperatura,
		    "vital_signs/body_temperature/any_event/temperature|unit": "°C",
		    "vital_signs/blood_pressure/any_event/systolic": sistolicniKrvniTlak,
		    "vital_signs/blood_pressure/any_event/diastolic": diastolicniKrvniTlak,
		    "vital_signs/indirect_oximetry:0/spo2|numerator": nasicenostKrviSKisikom*/
		};
		var parametriZahteve = {
		    ehrId: ehrId,
		    templateId: 'Vital Signs',
		    format: 'FLAT',
		    //committer: merilec
		};
		$.ajax({
		    url: baseUrl + "/composition?" + $.param(parametriZahteve),
		    type: 'POST',
		    contentType: 'application/json',
		    data: JSON.stringify(podatki),
		    success: function (res) {
		        $("#dodajMeritveVitalnihZnakovSporocilo").html(
              "<span class='obvestilo label label-success fade-in'>" +
              res.meta.href + ".</span>");
		    },
		    error: function(err) {
		    	$("#dodajMeritveVitalnihZnakovSporocilo").html(
            "<span class='obvestilo label label-danger fade-in'>Napaka '" +
            JSON.parse(err.responseText).userMessage + "'!");
		    }
		});
	}
}


/**
 * Pridobivanje vseh zgodovinskih podatkov meritev izbranih vitalnih znakov
 * (telesna temperatura, filtriranje telesne temperature in telesna teža).
 * Filtriranje telesne temperature je izvedena z AQL poizvedbo, ki se uporablja
 * za napredno iskanje po zdravstvenih podatkih.
 */
var grafT = null;
/*
function preberiMeritveVitalnihZnakov() {
	sessionId = getSessionId();

	var ehrId = $("#meritveVitalnihZnakovEHRid").val();
	var tip = $("#preberiTipZaVitalneZnake").val();

	if (!ehrId || ehrId.trim().length == 0 || !tip || tip.trim().length == 0) {
		$("#preberiMeritveVitalnihZnakovSporocilo").html("<span class='obvestilo " +
      "label label-warning fade-in'>Prosim vnesite zahtevan podatek!");
	} else {
		$.ajax({
			url: baseUrl + "/demographics/ehr/" + ehrId + "/party",
	    	type: 'GET',
	    	headers: {"Ehr-Session": sessionId},
	    	success: function (data) {
				var party = data.party;
				$("#rezultatMeritveVitalnihZnakov").html("<br/><span>Pridobivanje " +
          "podatkov za <b>'" + tip + "'</b> bolnika <b>'" + party.firstNames +
          " " + party.lastNames + "'</b>.</span><br/><br/>");
				if (tip == "telesna temperatura") {
					$.ajax({
  					    url: baseUrl + "/view/" + ehrId + "/" + "body_temperature",
					    type: 'GET',
					    headers: {"Ehr-Session": sessionId},
					    success: function (res) {
					    	if (res.length > 0) {
						    	var results = "<table class='table table-striped " +
                    "table-hover'><tr><th>Datum in ura</th>" +
                    "<th class='text-right'>Telesna temperatura</th></tr>";
						        for (var i in res) {
						            results += "<tr><td>" + res[i].time +
                          "</td><td class='text-right'>" + res[i].temperature +
                          " " + res[i].unit + "</td></tr>";
						        }
						        results += "</table>";
						        $("#rezultatMeritveVitalnihZnakov").append(results);
					    	} else {
					    		$("#preberiMeritveVitalnihZnakovSporocilo").html(
                    "<span class='obvestilo label label-warning fade-in'>" +
                    "Ni podatkov!</span>");
					    	}
					    },
					    error: function() {
					    	$("#preberiMeritveVitalnihZnakovSporocilo").html(
                  "<span class='obvestilo label label-danger fade-in'>Napaka '" +
                  JSON.parse(err.responseText).userMessage + "'!");
					    }
					});
				} else if (tip == "telesna teža") {
					$.ajax({
					    url: baseUrl + "/view/" + ehrId + "/" + "weight",
					    type: 'GET',
					    headers: {"Ehr-Session": sessionId},
					    success: function (res) {
					    	if (res.length > 0) {
						    	var results = "<table class='table table-striped " +
                    							"table-hover'><tr><th>Datum in ura</th>" +
                    							"<th class='text-right'>Telesna teža</th></tr>";
						        for (var i in res) {
						            results += "<tr><td>" + res[i].time +
                        				"</td><td class='text-right'>" + res[i].weight + " " 	+
                        				res[i].unit + "</td></tr>";
						        }
						        res.forEach(function (el, i, arr) {
						            var date = new Date(el.time);
						            el.date = date.getTime();
						        });
						        results += "</table>";
						        $("#rezultatMeritveVitalnihZnakov").append(results);
					    	} else {
					    		$("#preberiMeritveVitalnihZnakovSporocilo").html(
                    "<span class='obvestilo label label-warning fade-in'>" +
                    "Ni podatkov!</span>");
					    	}
					    	console.log(res);
					    	teze = res;
					    	if (grafT == null){
						    	grafT = new Morris.Area({
						            element: 'grafTeze',
						            data: res.reverse(),
						            xkey: 'time',
						            ykeys: ['weight'],
						            lineColors: ['#A0D468'],
						            labels: ['Teža'],
						            lineWidth: 1,
						            pointSize: 10,
						            hideHover: true,
						            behaveLikeLine: true,
						            smooth: false,
						            resize: true,
						            fillOpacity: 0.0,
						            //xLabels: "month",
						            xLabelFormat: function (x){
						                //var date = new Date(x);
						                return x.toLocaleDateString();
						            }
						        });
					    	}
					    	else {
					    		grafT.Area({
						            element: 'grafTeze',
						            data: res.reverse(),
						            xkey: 'time',
						            ykeys: ['weight'],
						            lineColors: ['#A0D468'],
						            labels: ['Teža'],
						            lineWidth: 1,
						            pointSize: 10,
						            hideHover: true,
						            behaveLikeLine: true,
						            smooth: false,
						            resize: true,
						            fillOpacity: 0.0,
						            //xLabels: "month",
						            xLabelFormat: function (x){
						                //var date = new Date(x);
						                return x.toLocaleDateString();
						            }
						        });
					    	}
					    },
					    error: function() {
					    	$("#preberiMeritveVitalnihZnakovSporocilo").html(
                  "<span class='obvestilo label label-danger fade-in'>Napaka '" +
                  JSON.parse(err.responseText).userMessage + "'!");
					    }
					});
					

				} else if (tip == "telesna temperatura AQL") {
					var AQL =
						"select " +
    						"t/data[at0002]/events[at0003]/time/value as cas, " +
    						"t/data[at0002]/events[at0003]/data[at0001]/items[at0004]/value/magnitude as temperatura_vrednost, " +
    						"t/data[at0002]/events[at0003]/data[at0001]/items[at0004]/value/units as temperatura_enota " +
						"from EHR e[e/ehr_id/value='" + ehrId + "'] " +
						"contains OBSERVATION t[openEHR-EHR-OBSERVATION.body_temperature.v1] " +
						"where t/data[at0002]/events[at0003]/data[at0001]/items[at0004]/value/magnitude<35 " +
						"order by t/data[at0002]/events[at0003]/time/value desc " +
						"limit 10";
					$.ajax({
					    url: baseUrl + "/query?" + $.param({"aql": AQL}),
					    type: 'GET',
					    headers: {"Ehr-Session": sessionId},
					    success: function (res) {
					    	var results = "<table class='table table-striped table-hover'>" +
                  "<tr><th>Datum in ura</th><th class='text-right'>" +
                  "Telesna temperatura</th></tr>";
					    	if (res) {
					    		var rows = res.resultSet;
						        for (var i in rows) {
						            results += "<tr><td>" + rows[i].cas +
                          "</td><td class='text-right'>" +
                          rows[i].temperatura_vrednost + " " 	+
                          rows[i].temperatura_enota + "</td>";
						        }
						        results += "</table>";
						        $("#rezultatMeritveVitalnihZnakov").append(results);
					    	} else {
					    		$("#preberiMeritveVitalnihZnakovSporocilo").html(
                    "<span class='obvestilo label label-warning fade-in'>" +
                    "Ni podatkov!</span>");
					    	}

					    },
					    error: function() {
					    	$("#preberiMeritveVitalnihZnakovSporocilo").html(
                  "<span class='obvestilo label label-danger fade-in'>Napaka '" +
                  JSON.parse(err.responseText).userMessage + "'!");
					    }
					});
				}
	    	},
	    	error: function(err) {
	    		$("#preberiMeritveVitalnihZnakovSporocilo").html(
            "<span class='obvestilo label label-danger fade-in'>Napaka '" +
            JSON.parse(err.responseText).userMessage + "'!");
	    	}
		});
	}
}
*/
	var seznamReceptov = [];
	function prehrambek(){
		//console.log("to");
		var kljucnaBeseda = $("#nastaviKljucnoBesedo").val();
		if (kljucnaBeseda == "")
			kljucnaBeseda = "healthy";
		var povezava = "https://api.edamam.com/search?q=healthy&app_id=929cb6d5&app_key=ff50010d319758c7e468a02185c90826";
		var diet = "";
		if (zadnjiBMI > 25.00){
			diet = "&diet=low-fat";
		}
		else if (zadnjiBMI > 18.50){
			diet = "&diet=balanced";
		}
		else {
			diet = "&diet=high-protein";
		}
		//console.log (povezava+diet);
		//url: "https://api.edamam.com/search?q=healthy&app_id=929cb6d5&app_key=ff50010d319758c7e468a02185c90826&from=0&to=5"+diet,
		$.ajax({
			url: "https://api.edamam.com/search?q="+kljucnaBeseda+"&app_id=929cb6d5&app_key=ff50010d319758c7e468a02185c90826&from=0&to=5"+diet,
			//url: "test.json",
	    	type: 'GET',
	    	dataType: 'jsonp',
	    	success: function (data) {
	    		console.log (data);
	    		data = data.hits;
	    		if (data.length>0){
		    		seznamReceptov = data;
		    		console.log (seznamReceptov);
		    		var results = "<p><b>Iskanje z ključno besedo: </b>"+kljucnaBeseda+"</p><table align='middle'><tr>";
		    		for (var i = 0; i<data.length; i++) {
		    			results += "<tr><td><button type='button' class='btn btn-success btn-sm' onclick='prikaziHrano("+i+")'>"+data[i].recipe.label+"</button></td></tr>";
		    		}
		    		
		    		//results = "</tr><tr>";
		    		/*for (var i = 0; i<data.length; i++) {
		    			results += "<td><img src='"+seznamReceptov[i].recipe.image+"' alt='Slika'></td>";
		    		}*/
		    		
		    		results += "</table>";
		    		//console.log (results);
		    		$("#seznamReceptov").html(results);
	    		}
				else {
					$("#seznamReceptov").html("Ni zadetkov, poskusite drugo ključno besedo ali pa pustite polje prazno.");
				}
	    	}
		});
    }
    function prikaziHrano(zapStevilka){
    	var img = "<img class = 'img1' src='"+seznamReceptov[zapStevilka].recipe.image+"' alt='Slika'>"
    	var sestavine = "<table class='table" +
					"table-hovedar'><tr><th>Sestavine</th></tr>";
		var dnevneKalorije = "<table class='table" +
					"table-hovedar table-striped'><tr><th>Zadosti povprečnim dnevnim potrebam</th></tr>";
		//console.log (seznamReceptov[zapStevilka].recipe.totalDaily.toString());
		if(seznamReceptov[zapStevilka].recipe.totalDaily.CA){
			dnevneKalorije += "<tr><td>Kalcij:</td><td>"+seznamReceptov[zapStevilka].recipe.totalDaily.CA.quantity.toFixed(2)+"%</td></tr>"
		}
		if(seznamReceptov[zapStevilka].recipe.totalDaily.CHOCDF){
			dnevneKalorije += "<tr><td>Ogljikovi Hidrati:</td><td>"+seznamReceptov[zapStevilka].recipe.totalDaily.CHOCDF.quantity.toFixed(2)+"%</td></tr>"
		}
		if(seznamReceptov[zapStevilka].recipe.totalDaily.CHOLE){
			dnevneKalorije += "<tr><td>Holesterol:</td><td>"+seznamReceptov[zapStevilka].recipe.totalDaily.CHOLE.quantity.toFixed(2)+"%</td></tr>"
		}
		if(seznamReceptov[zapStevilka].recipe.totalDaily.FAT){
			dnevneKalorije += "<tr><td>Maščobe:</td><td>"+seznamReceptov[zapStevilka].recipe.totalDaily.FAT.quantity.toFixed(2)+"%</td></tr>"
		}
		if(seznamReceptov[zapStevilka].recipe.totalDaily.VITA_RAE){
			dnevneKalorije += "<tr><td>Vitamin A:</td><td>"+seznamReceptov[zapStevilka].recipe.totalDaily.VITA_RAE.quantity.toFixed(2)+"%</td></tr>"
		}
		if(seznamReceptov[zapStevilka].recipe.totalDaily.VITC){
			dnevneKalorije += "<tr><td>Vitamin C:</td><td>"+seznamReceptov[zapStevilka].recipe.totalDaily.VITC.quantity.toFixed(2)+"%</td></tr>"
		}
    	for (var i = 0; i<seznamReceptov[zapStevilka].recipe.ingredientLines.length; i++) {
	    	sestavine += "<tr><td>" + seznamReceptov[zapStevilka].recipe.ingredientLines[i] + "</td>";
	    }
	    dnevneKalorije += "</table>";
	    sestavine += "</table>";
    	var izpis = "<div class='panel panel-success'><div class='panel-heading'><div align='middle'><b>"+seznamReceptov[zapStevilka].recipe.label+"</b></div></div><div class='panel-body'><table><tr><td class='tab1'>"+img+"</td><td class = 'tab1'>"+sestavine+"</td><td class = 'tab1'>"+dnevneKalorije+"</td><td><p><i>Več na:    </i><a target='_blank' href='"+seznamReceptov[zapStevilka].recipe.shareAs+"'><img style='width:50px;height:50px;' src='link_pic.png' alt='Povezava'></a></p><p><i>(Povezava se odpre v novem zavihku)</i></p></td></tr></table></div></div>";
    	$("#podrobnostiRecepta").html(izpis);
    }
$(document).ready(function() {

  $('#izberiBolnika').change(function() {
  	//console.log($(this).val());
  	var tip = $(this).val();
  	if (tip != "-"){
  		trenutniID = "";
  		$("#pacientovID").html("");
  		$("#patient-name").html("");
  		$("#preberiSporocilo").html("");
  		$("#patient-age").html("");
  		$("#patient-gender").html("");
  		$("#patient-dob").html("");
  		$("#BMI").html("Ni podatkov");
  		$("#Klasifikacija").html("Ni podatkov");
  		myChart.data.datasets[0].data = [];
  		myChart2.data.datasets[0].data = [];
		myChart2.data.datasets[1].data = [];
  		myChart.update();
  		myChart2.update();
  		$("#mestoZaGumb").html("<i>Za uporabo te funkcije najprej izberite uporabnika</i>");
  		$("#rezultatMeritveVitalnihZnakov").html("");
  	}
  	if (tip != "-" && tip != "")
    	nastaviEHRBolnika(tip);
  });
  /*
  Graf = Morris.Area({
    element: 'grafTeze',
    data: podatkiZaGraf.reverse(),
    xkey: 'time',
    ykeys: ['weight'],
    lineColors: ['#A0D468'],
    labels: ['Teža'],
    lineWidth: 1,
    pointSize: 5,
    hideHover: true,
    behaveLikeLine: true,
    smooth: false,
    resize: true,
    fillOpacity: 0.0,
    //xLabels: "month",
    xLabelFormat: function (x){
        //var date = new Date(x);
        return x.toLocaleDateString();
    }
  });
  GrafBMI = Morris.Area({
    element: 'grafBMI',
    data: podatkiZaGrafBMI.reverse(),
    xkey: 'time',
    ykeys: ['BMI', "BMI_MAKS"],
    lineColors: ['#A0D468', '#FF0000'],
    labels: ['ITM'],
    lineWidth: 1,
    pointSize: 5,
    hideHover: true,
    behaveLikeLine: true,
    smooth: false,
    resize: true,
    fillOpacity: 0.0,
    //xLabels: "month",
    xLabelFormat: function (x){
        //var date = new Date(x);
        return x.toLocaleDateString();
    }
  });
  */
	//var ctx = ;
	
	//var timeFormat = 'YYYY-MM-DDThh:mm:ss.sssTZD';
	myChart = new Chart(document.getElementById("grafBMI"), {
	    type:    'line',
        data:    {
            datasets: [
                {
                    label: "ITM",
                    data: podatkiZaGrafBMI,
                    fill: false,
                    borderColor: 'blue',
                    xAxisID: 'casi'
                },
                {
                    label: "Debelost",
                    data: [30 ,30],
                    fill: false,
                    borderColor: 'red',
                    xAxisID: 'all'
                },
                {
                    label: "Podhranjenost",
                    data: [18.5 ,18.5],
                    fill: false,
                    borderColor: 'grey',
                    xAxisID: 'all'
                },
                {
                    label: "Povišana telesna teža",
                    data: [25 ,25],
                    fill: false,
                    borderColor: '#FF4500',
                    xAxisID: 'all'
                }
            ]
            
        },
        options: {
            responsive: true,
            scales:     {
                xAxes: [
                	{
	                	id: 'casi',
	                    type:       "time",
	                    distribution: 'series',
	                    time: {
	                        //format: timeFormat,
	                        tooltipFormat: 'll'
	                    },
	                    scaleLabel: {
	                        display:     true,
	                        labelString: 'Čas'
	                    }
	                },
	                {
	                	id: 'all',
	                }
                ],
                yAxes: [{
                    scaleLabel: {
                        display:     true,
                        labelString: 'ITM',
                    },
                    ticks: {
                    	beginAtZero:true
                	}
                }]
            }
        }
	});
	myChart2 = new Chart(document.getElementById("grafTeze"), {
	    type:    'line',
        data:    {
            datasets: [
                {
                    label: "Teža",
                    data: podatkiZaGrafTeza,
                    fill: false,
                    borderColor: 'blue',
                    xAxisID: 'casi',
                    yAxisID: 'kg'
                },
                {
                    label: "Visina",
                    data: podatkiZaGrafVisina,
                    fill: false,
                    borderColor: 'red',
                    xAxisID: 'casi',
                    yAxisID: 'cm'
                }
            ]
            
        },
        options: {
            responsive: true,
            scales:     {
                xAxes: [
                	{
	                	id: 'casi',
	                    type:       "time",
	                    distribution: 'series',
	                    time: {
	                        //format: timeFormat,
	                        tooltipFormat: 'll'
	                    },
	                    scaleLabel: {
	                        display:     true,
	                        labelString: 'Čas'
	                    }
	                }
                ],
                yAxes: [
                	{
                		id: 'kg',
	                    scaleLabel: {
	                        display:     true,
	                        labelString: 'kg',
	                    },
	                    position: 'left',
	                    ticks: {
	                    	beginAtZero:true
	                	}
                	},
                	{
                		id: 'cm',
	                    scaleLabel: {
	                        display:     true,
	                        labelString: 'cm',
	                    },
	                    position: 'right',
	                    ticks: {
	                    	beginAtZero:true
	                	}
	                	
	                }
                ]
            }
        }
	});
	
	/*
	myChart = new Chart(ctx, {
	    type:    'line',
        data:    {
            datasets: [
                {
                    label: "BMI",
                    data: [{
                        x: "2000-01-01T01:01:01.000+00:00", y: 175
                    }, {
                        x: "2001-01-01T01:01:01.000+00:00", y: 175
                    }, {
                        x: "2002-01-01T01:01:01.000+00:00", y: 178
                    }
                    ],
                    fill: false,
                    borderColor: 'red'
                },
                {
                    label: "UK Dates",
                   data: [{
                        x: "2000-01-01T01:01:01.000+00:00", y: 175
                    }, {
                        x: "2001-01-01T01:01:01.000+00:00", y: 175
                    }, {
                        x: "2002-01-01T01:01:01.000+00:00", y: 178
                    }
                    ],
                    fill:  false,
                    borderColor: 'blue'
                }
            ]
        },
        options: {
            responsive: true,
            title:      {
                display: true,
                text:    "Chart.js Time Scale"
            },
            scales:     {
                xAxes: [{
                    type:       "time",
                    time:       {
                        format: timeFormat,
                        tooltipFormat: 'll'
                    },
                    scaleLabel: {
                        display:     true,
                        labelString: 'Date'
                    }
                }],
                yAxes: [{
                    scaleLabel: {
                        display:     true,
                        labelString: 'value'
                    }
                }]
            }
        }
	});
	*/
/**
   * Napolni testne vrednosti (ime, priimek in datum rojstva) pri kreiranju
   * EHR zapisa za novega bolnika, ko uporabnik izbere vrednost iz
   * padajočega menuja (npr. Pujsa Pepa).
   */
  $('#preberiPredlogoBolnika').change(function() {
    $("#kreirajSporocilo").html("");
    var podatki = $(this).val().split(",");
    $("#kreirajIme").val(podatki[0]);
    $("#kreirajPriimek").val(podatki[1]);
    $("#kreirajDatumRojstva").val(podatki[2]);
  });

  /**
   * Napolni testni EHR ID pri prebiranju EHR zapisa obstoječega bolnika,
   * ko uporabnik izbere vrednost iz padajočega menuja
   * (npr. Dejan Lavbič, Pujsa Pepa, Ata Smrk)
   */
	$('#preberiObstojeciEHR').change(function() {
		$("#preberiSporocilo").html("");
		$("#preberiEHRid").val($(this).val());
	});

  /**
   * Napolni testne vrednosti (EHR ID, datum in ura, telesna višina,
   * telesna teža, telesna temperatura, sistolični in diastolični krvni tlak,
   * nasičenost krvi s kisikom in merilec) pri vnosu meritve vitalnih znakov
   * bolnika, ko uporabnik izbere vrednosti iz padajočega menuja (npr. Ata Smrk)
   */
	$('#preberiObstojeciVitalniZnak').change(function() {
		$("#dodajMeritveVitalnihZnakovSporocilo").html("");
		var podatki = $(this).val().split("|");
		$("#dodajVitalnoEHR").val(podatki[0]);
		$("#dodajVitalnoDatumInUra").val(podatki[1]);
		$("#dodajVitalnoTelesnaVisina").val(podatki[2]);
		$("#dodajVitalnoTelesnaTeza").val(podatki[3]);
		$("#dodajVitalnoTelesnaTemperatura").val(podatki[4]);
		$("#dodajVitalnoKrvniTlakSistolicni").val(podatki[5]);
		$("#dodajVitalnoKrvniTlakDiastolicni").val(podatki[6]);
		$("#dodajVitalnoNasicenostKrviSKisikom").val(podatki[7]);
		$("#dodajVitalnoMerilec").val(podatki[8]);
	});

  /**
   * Napolni testni EHR ID pri pregledu meritev vitalnih znakov obstoječega
   * bolnika, ko uporabnik izbere vrednost iz padajočega menuja
   * (npr. Ata Smrk, Pujsa Pepa)
   */
	$('#preberiEhrIdZaVitalneZnake').change(function() {
		$("#preberiMeritveVitalnihZnakovSporocilo").html("");
		$("#rezultatMeritveVitalnihZnakov").html("");
		$("#meritveVitalnihZnakovEHRid").val($(this).val());
	});

});